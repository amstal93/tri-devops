# DevOps challenge

[![pipeline status](https://gitlab.com/scoday1/tri-devops/badges/master/pipeline.svg)](https://gitlab.com/scoday1/tri-devops/commits/master)

## Introduction
The challenge here is to build a docker image with ci, then use this image to
build and host an mkdocs site. At first it took some time to sort out mkdocks
since I have not used it before.

## Background
Produce and serve a website inside a container with mkdocs installed inside the
container. This site once finished will ultimately run on a local volume provisioned
with docker using the:  

`> docker volume create $volname`  

Since the instructions require an output directory we sould make that:

`> mkdir $outputdir`   

This will map to our container once we have built the site. This will also be used
to extract the tarball. 

### Feedback
Version one was over engineering 101 and I did not specifically tick off all the
requirements. 

## Goals
- [x] Create a GIT project that builds the docker image in a pipeline (CI/CD)
- [x] Docker image once built should take local directory input to serve the website
- [x] Local directory is the root of mkdocs project for site creation
- [ ] Use ci/cd to test the mkdocs container

### Build and Serve Instructions 
*Expected docker execution*  

First we clone the repo:

`> git clone git@gitlab.com:scoday1/tri-devops.git && cd tri-devops`   

Make a directory for the ZIP file:
`> mkdir ~/triz`

Make a volume:
`> docker volume create tri-data`

When you run:
`> docker volume create tri-data`   
You should see:
`tri-data`

To be sure:
`> docker volume ls | grep tri-data`   
`local               tri-data`   

Then build the image as the CI/CD does:

`> docker build -t $container_name .`   

Then produce the site, keep in mind `$home_base` refers to your structure e.g.
Linux: /home/$user
OSX: /Users/$user

`> docker run -v tri-data:/tri-data -v $home_base/$user/triz:/triz $container_name /produce`  

This should:
* Pass an argument and read local dir that has the project
* Internally use mkdocs (on the docker container)
* Write to stdout a tarball which would include
  * Static website
  * index.html
  * resources
* Exit

Suggested Method to serve:

`> docker run -d -p 8000:8000 -v tri-data:/tri-data -v /$home_base/$user/triz:/triz $container_name /serve`  

Browse to http://localhost:8000 and you should see the site output.

### Back to team
- [x] Code with comments about solution  
- [x] gitlab-ci.yml build / test  
- [x] README.md  
- [x] Git History (login to gitlab?)  
- [x] Zip with first.last.tgz -> email  
- [x] How long  

## Summary
In the end it took me about 2 hours of poking around and finally realizing how to do docker in docker in gitlab. All told I probably spend several hours researching best practices for CI/CD with docker and in the end kept it simple. I went with shell scripts just because I knew they would work - they are versionable and they can be modified to do other things and stuff and used elsewhere. 

What I found took the longest for some odd reason was understanding how mkdocs worked. I do not know why I had a mental freeze when I was looking at it. The reality is it is quite simple and straight forward. The other thing that took some time was figuring out if the Challenege was okay with me thinking, e.g. putting in some scripts to make things "wicked" easy, etc..

Diagram:
![](tri-devops-docker.png)
